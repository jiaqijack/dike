Dike
====

Dike is a Redis-based API for voting data on web page links, articles, or questions. Dike allows applications to rank
web contents based on a score related to votes on those contents. 

In ancient Greek culture, Dike (Greek: Δίκη, "Justice") is the goddess of justice and the spirit of moral order and fair judgement

In recent years, a growing number of sites have offered the ability to vote on web page links, articles, or questions, 
including sites such as [reddit](https://www.reddit.com/) and [Stack Overflow](https://stackoverflow.com/).

![](https://jiaqijack.bitbucket.io/Dike/README/example-small.png)

[Dike](https://bitbucket.org/jiaqijack/dike) allows ranking and displaying of those resources based on a "score". A
typical use case of [Dike](https://bitbucket.org/jiaqijack/dike) is the following:

Let's say that 1,000 articles are submitted each day. Of those 1,000 articles, about 50 of them are interesting enough
that we want them to be in the top-100 articles for at least one day. All of those 50 articles will receive at least 200
up votes. [Dike](https://bitbucket.org/jiaqijack/dike) doesn't support down votes at this moment.

In [Dike](https://bitbucket.org/jiaqijack/dike)'s ranking assumes that scores go down over time and considers both the
posting time and current time. The score of an item is a function of the time that the article was posted, plus a
constant multiplier times the number of votes that the article has received.

To compute time, [Dike](https://bitbucket.org/jiaqijack/dike) uses the number of seconds since January 1, 1970, in the
UTC time zone, which is commonly referred to as Unix time. **For the aforementioned constant,
[Dike](https://bitbucket.org/jiaqijack/dike) takes the number of seconds in a day (86400) divided by the number of votes 
required (200) to last a full day, which gives us 432 "points" added to the score per vote**.

    java -jar target/Dike-<version>-SNAPSHOT.jar
    
    


