package org.bitbucket.jiaqijack.Dike;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DikeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DikeApplication.class, args);
	}
}
