/*
 * Copyright 刘珈奇
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jiaqijack.Dike.controller;

import org.bitbucket.jiaqijack.Dike.datastore.DataStore;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller that handles all HTTP health-check requests
 */
@RestController
public class VotingController {


    private final DataStore dataStore;

    /**
     * Method that is called when HTTP GET request to "/healthcheck" is sent.
     *
     * @return a JSON string in webservice response
     */
    @GetMapping("/vote")
    public String check() {
        return "OK";
    }


}
