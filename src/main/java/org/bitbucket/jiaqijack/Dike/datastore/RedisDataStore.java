/*
 * Copyright 刘珈奇
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jiaqijack.Dike.datastore;

import static java.util.AbstractMap.SimpleImmutableEntry;
import static org.bitbucket.jiaqijack.time.TimeUtils.getCurrentUnixTimeInSeconds;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Each article contains the following information:
 * <ul>
 *     <li> title
 *     <li> the link to the article
 *     <li> who posted it
 *     <li> the time it was posted
 *     <li> the number of votes received
 * </ul>
 * Such information is stored using Redis HASH. Below is an example article:
 * <p>
 * +---------------+------------------------------------+
 * | article:92617                                 hash |
 * +---------------+------------------------------------+
 * | title         | Go to statement considered harmful |
 * | link          | http://goo.gl/kZUSu                |
 * | poster        | user:83271                         |
 * | time          | 1331382699.33                      |
 * | votes         | 528                                |
 * +---------------+------------------------------------+
 * <p>
 * To store a sorted set of articles themselves, {@link RedisDataStore} uses a ZSET, which keeps items ordered by the
 * item scores. Article ID is is the member, with the ZSET score being the article score itself.
 * <p>
 * There is another ZSET with the score being just the times that the articles were posted, which gives us an option of
 * browsing articles based on article score or time. Here is a small example of time- and score-ordered article ZSETs:
 * <p>
 * +-----------------+---------------+      +-----------------+---------------+
 * | time:                      zset |      | score:                     zset |
 * +-----------------+---------------+      +-----------------+---------------+
 * | article:100408  | 1332065417.47 |      | article:100635  | 1332164063.49 |
 * | article:100635  | 1332075503.49 |      | article:100408  | 1332174713.47 |
 * | article:100716  | 1332082035.26 |      | article:100716  | 1332225027.26 |
 * +-----------------+---------------+      +-----------------+---------------+
 * <p>
 * In order to prevent users from voting for the same article more than once, there is also an unique listing of users
 * who have voted for each article. For this, {@link RedisDataStore} utilizes a SET for each article, and stores the
 * member IDs of all users who have voted on the given article. An example SET of users who have voted on an article is
 * shown below:
 * <p>
 * +----------------------------+
 * | voted:100408           set |
 * +----------------------------+
 * | user:234487                |
 * | user:253378                |
 * | user:364680                |
 * | user:132097                |
 * | user:350917                |
 * +----------------------------+
 * <p>
 * For the sake of memory use over time, after a week users can no longer vote on an article and its score is fixed.
 * After that week has passed, {@link RedisDataStore} deletes the SET of users who have voted on the article.
 *
 * <h3>Handle Voting</h3>
 * When someone tries to vote on an article, {@link RedisDataStore} first verifies that the article was posted within
 * the last week by checking the article's post time with ZSCORE. If we still have time, then add the user to the
 * article's voted SET with SADD. Finally, if the user didn't previously vote on that article, increment the score of
 * the article with ZINCRBY (a command that increments the score of a member), and update the vote count in the HASH
 * with HINCRBY (a command that increments a value in a hash).
 *
 * <h3>Posting Articles</h3>
 * To post an article, create an article ID by incrementing a counter with INCR. Then create the voted SET by adding the
 * poster's ID to the SET with SADD. To ensure that the SET is removed after one week, {@link RedisDataStore} gives it
 * an expiration time with the EXPIRE command, which lets Redis automatically delete it. Then store the article
 * information with HMSET. Finally, add the initial score and posting time to the relevant ZSETs with ZADD.
 *
 * <h3>Fetching Articles</h3>
 * To fetch the current top-scoring or most recent articles, use ZRANGE to fetch the article IDs, then make calls to
 * HGETALL to fetch information about each article.
 * <p>
 * TODO - Support "topical groups"
 */
public class RedisDataStore implements DataStore {

    private static final String SEPARATOR = ":";
    private static final String TIME_NAMESPACE = "time" + SEPARATOR;
    private static final String SCORE_NAMESPACE = "score" + SEPARATOR;
    private static final String ARTICLE_NAMESPACE = "article" + SEPARATOR;
    private static final String VOTED_SET_NAMESPACE = "voted" + SEPARATOR;

    private static final int NUM_ARTICLES_PER_PAGE = 25;
    private static final int ONE_WEEK_IN_SECONDS = 60 * 60 * 24 * 7;

    private static final int VOTED_SET_EXPIRY_IN_SECONDS = 60 * 60 * 24 * 7;

    private final JedisPool jedisPool;

    public RedisDataStore(final JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    @Override
    public String postArticle(final String title, final String link, final String user) {
        try (Jedis redisClient = getJedisPool().getResource()) {
            // Creates a new article ID by incrementing a counter with INCR.
            String articleId = redisClient.incr(ARTICLE_NAMESPACE).toString();

            // Start with the posting user having voted for the article, and set the article voting information to
            // automatically expire in a week
            String nameSpacedVotedSetId = namespacedVotedSetId(articleId);
            redisClient.sadd(nameSpacedVotedSetId, user);
            redisClient.expire(nameSpacedVotedSetId, VOTED_SET_EXPIRY_IN_SECONDS);

            // Create the article hash
            String nameSpacedArticleId = namespacedArticleId(articleId);
            redisClient.hmset(
                    nameSpacedArticleId,
                    Stream.of(
                            new SimpleImmutableEntry<>("title", title),
                            new SimpleImmutableEntry<>("link", link),
                            new SimpleImmutableEntry<>("poster", user),
                            new SimpleImmutableEntry<>("time", String.valueOf(getCurrentUnixTimeInSeconds())),
                            new SimpleImmutableEntry<>("votes", "1")
                    ).collect(
                            Collectors.collectingAndThen(
                                    Collectors.toMap(
                                            Map.Entry::getKey,
                                            Map.Entry::getValue
                                    ),
                                    Collections::unmodifiableMap
                            )
                    )
            );

            // Add the article to the time and score ordered ZSETs.
            long now = getCurrentUnixTimeInSeconds();
            redisClient.zadd(SCORE_NAMESPACE, ScoreGenerator.newArticleInitialScore(), nameSpacedArticleId);
            redisClient.zadd(TIME_NAMESPACE, now, nameSpacedArticleId);

            return articleId;
        }
    }

    @Override
    public void voteArticle(final String articleId, final String user) {
        String namespacedArticleId = namespacedArticleId(articleId);

        // Calculate the cutoff time for voting.
        long cutoff = getCurrentUnixTimeInSeconds() - ONE_WEEK_IN_SECONDS;

        try (Jedis redisClient = getJedisPool().getResource()) {
            // Check to see if the article can still be voted on
            if (redisClient.zscore(TIME_NAMESPACE, namespacedArticleId) < cutoff) {
                return;
            }

            // If the user hasn’t voted for this article before, increment the article score and vote count.
            // TODO - SADD, ZINCRBY, and HINCRBY calls should be in a transaction
            if (redisClient.sadd(namespacedVotedSetId(articleId), user) == 1) {
                redisClient.zincrby(SCORE_NAMESPACE, ScoreGenerator.scorePerVote(), namespacedArticleId);
                redisClient.hincrBy(namespacedArticleId, "votes", 1L);
            }
        }
    }

    @Override
    public List<Map.Entry<String, Map<String, String>>> getTopArticlesOnPage(final int page) {
        // Set up the start and end indexes for fetching the articles.
        int start = (page - 1) * NUM_ARTICLES_PER_PAGE;
        int end = start + NUM_ARTICLES_PER_PAGE - 1;

        try (Jedis redisClient = getJedisPool().getResource()) {
            // Fetch the article ids.
            Set<String> articleIdsInRequestedPage = redisClient.zrevrange(SCORE_NAMESPACE, start, end);

            // Get the article information from the list of article ids.
            return articleIdsInRequestedPage.stream()
                    .map(id -> new SimpleImmutableEntry<>(id, redisClient.hgetAll(id)))
                    .collect(
                            Collectors.collectingAndThen(
                                    Collectors.toList(),
                                    Collections::unmodifiableList
                            )
                    );
        }
    }

    private static String namespacedArticleId(String id) {
        return ARTICLE_NAMESPACE + id;
    }

    private static String namespacedVotedSetId(String id) {
        return VOTED_SET_NAMESPACE + id;
    }

    private JedisPool getJedisPool() {
        return jedisPool;
    }
}
